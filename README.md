# Animated Rating View

<t>A <b>SwiftUI</b> demo project.</t> <br/><br/>
Mask and overlay used for giving color to star(s). <br/><br/>
Animation starts after taping to the desired star. <br/>
For instance; if 3rd start is tapped, first 3 star will be filled with color. 

<table>
<thead>
  <tr>
      <th colspan="2">
     <div align="center">Demo Video</div>
      </th>
  </tr>
</thead>
<tbody>
  <tr>
     <td>
      ![ ](Screenshots/demo.mp4)
      </td>
  </tr>
</tbody>
</table>
